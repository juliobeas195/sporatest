# Sporatest

El proyecto fué generado con [Angular CLI](https://github.com/angular/angular-cli) versión 12.1.0.

## Requerimientos

Antes de empezar con Angular, se requiere de: 
- [ ] Editor de código [Visual Code](https://code.visualstudio.com/download) o de su preferencia. 

- [ ] [Contar con NPM](https://docs.npmjs.com/getting-started).

Después se hace la instalación de Angular CLI

- [ ] [Angular CLI](https://angular.io/cli) utilizando  `npm install -g @angular/cli`

## Correr el programa

Asegurarse de ejecutar primero `npm install` y después `ng serve` para correr la aplicación web. Se podrá consultar en `http://localhost:4200/` desde cualquier navegador. 

## Componentes principales

Los componentes principales son los escenciales para satisfacer los requerimientos de la prueba técnica del programa de SPORA. A continuación se enlistan cada unos de ellos. 
- [ ] Servicio de usuarios. [user.service.ts](src/app/services/user.service.ts) Este servicio es escencial para guardar y recuperar los registros, de momento se guardan en el localstorage del navegador. 
- [ ] Componente de inicial. [login.component.ts](src/app/components/login/login/login.component.ts) Este componente es el que se muestra inicialmente, donde se inicia sesión y si existe avanza a la sección donde se muestran todos los registros caso contrario se redirige a registrar un nuevo usuario.
- [ ] Componente de todos los registros. [home-usuario.component.ts](src/app/components/usuario/home-usuario/home-usuario.component.ts) Este componente es el que se muestra todos los registros guardados, permite eliminar cada uno de ellos, en caso que se eliminen todos se redirigirá al componente de registro. 
- [ ] Componente de registro. [cru-usuario.component.ts](src/app/components/usuario/cru-usuario/cru-usuario.component.ts) Este componente contine inputs para completar con los datos del usuario además se selecciona una imagen como foto de pérfil para el usuario, cada uno de los inputs contiene sus respectivas validaciones. Al tener un registro exitoso se redirecciona al componente con todos los registros. 