import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'image'
})
export class ImagePipe implements PipeTransform {

  base64Image: any;
  
  constructor(private sanitizer: DomSanitizer) {

  }
  transform(value: any): any {

    if ( !value ) {
      return undefined;
    } 
    if ( !value.data) {
      return "https://www.w3schools.com/howto/img_avatar.png";
    } else {
      const base64Flag = 'data:image/png;base64,';

      return this.sanitizer.bypassSecurityTrustUrl(base64Flag + value.data);
    }
  }
}
