import { Injectable } from '@angular/core';
import { Usuario } from '../components/usuario/cru-usuario/cru-usuario.component';

/**
 * Servicio que se encarga del CRUD de usuarios.
 * Actualmente los usuarios se guardan en local storage del navegador en [usuarios]
 * 
 */

@Injectable({
  providedIn: 'root'
})
export class UserService {


  users: Array<Usuario> = [];

  constructor() {
    this.actualizarDatos();
    
  }

  /** Método que se encarga de recuperar los datos almacenados **/
  actualizarDatos() {
    if ( localStorage.usuarios ) {
      this.users =  JSON.parse(localStorage.usuarios);
    }
  }
  /** Método que se encarga de guardar un nuevo Usuario (Recibe item de tipo Usuario) **/
  agregarUsuario(item: Usuario) {

    item.correo = item.correo.toLowerCase();

    this.users.push(item);
    localStorage.setItem("usuarios", JSON.stringify(this.users));
  }
  /** Función que se encarga de enviar los usuarios **/
  obtenerUsuarios() {
    return this.users;
  }
  /** Función que se encarga de enviar el último usuario **/
  obtenerUltimo(): Usuario {
    let ultimoUsuario: Usuario = {nombre: '', imagen: {nombre: '', data: ''}, apellidos: '', direccion: '', correo: '', telefono: 0};

    if( this.users.length > 0 ) {
      ultimoUsuario = this.users[this.users.length - 1];
    }
    return ultimoUsuario;
  }
  /** Función que se encarga de regresar un usuario que se busca por correo. (Recibe un string para hacer la búsqueda) **/
  buscarCorreo(correo: string): any {

    let usuarioEncontrado: any = undefined;
    this.actualizarDatos();
    this.users.forEach((user: Usuario) => {
      if ( user.correo === correo) {
        usuarioEncontrado = user;
      }
    });

    return usuarioEncontrado;
  }
  /** Metodo que se encarga de editar un usuario existente. (Recibe posición en el arreglo y la nueva información.) **/
  editarUsuario(i: number, newInfo: Usuario) {
    if ( i >= 0 && i < this.users.length) {

      this.users[i] = newInfo;

      localStorage.setItem("usuarios", JSON.stringify(this.users));
    }
  }
  /** Método que se encarga de eliminar un usuario. (Recibe i; posición en el arreglo a eliminar)*/
  eliminarUsuario(i: number) {
    if ( i >= 0 && i < this.users.length) {

      this.users.splice(i, 1);

      localStorage.setItem("usuarios", JSON.stringify(this.users));
    }
  }
}
