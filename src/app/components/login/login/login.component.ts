import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { Usuario } from '../../usuario/cru-usuario/cru-usuario.component';
/**
 * Componente inicial de la página (Inicio de sesión).
 * 
 * - Se encarga de recuperar último usuario registrado, si lo encuentra avanza a la vista de todos los usuarios
 *  de no ser así se avanza a la página para registrar usuario. 
 * 
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  correo: string = '';

  // Bandera que se activa al consultar los usuarios registrados. 
  consultando: boolean = false;
  ultimoUsuario: Usuario;

  constructor(public router: Router, public user_service: UserService) { 
    this.ultimoUsuario = user_service.obtenerUltimo();
  }

  ngOnInit(): void {

    this.correo = this.ultimoUsuario.correo;

  }

  /** Método que se ejecuta después de hacer el clic en "ingresar". Si el correo existe se dirige a ver todos los usuarios, de no ser así se dirige al registro. */
  ingresar(): void {
    this.consultando = true;

    let encontrado = this.user_service.buscarCorreo(this.correo);

    if ( encontrado ) {
      this.router.navigate(['usuarios']);
    } else {
      this.router.navigate(['usuario/nuevo']);
    }
    
  }

}
