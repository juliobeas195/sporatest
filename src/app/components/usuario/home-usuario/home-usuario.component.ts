import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';
import { ImagUser, Usuario } from '../cru-usuario/cru-usuario.component';

/**
 * Componente donde se muestra a todos los usuarios.
 * 
 * - Se encarga de recuperar todos los usuarios registrados, se muestra la información, la imagen superior pertenece al último registro.
 * - De momento solo se pueden eliminar usuarios, si se eliminan todos se avanza a la página de registro. 
 * - Contiene un botón que permite agregar nuevos usuarios. 
 * - También contiene un botón que regresa al inicio del programa. 
 * 
 */
@Component({
  selector: 'app-home-usuario',
  templateUrl: './home-usuario.component.html',
  styleUrls: ['./home-usuario.component.css']
})
export class HomeUsuarioComponent implements OnInit {


  usuarios: Array<Usuario> = [];
  imagenSeleccionada: ImagUser = { nombre: '', data: ''};

  constructor(private user_service: UserService, public router: Router) { }

  ngOnInit(): void {
    this.consultarInformacion();
    
  }

  /** Método para consultar a todos los usuarios */
  consultarInformacion() {
    this.usuarios = this.user_service.obtenerUsuarios();
    if(this.usuarios.length > 0) {
      this.imagenSeleccionada = this.usuarios[this.usuarios.length - 1].imagen;
    } else {
      this.router.navigate(['usuario/nuevo']);
    }
  }

  /* Método para eliminar a un usuario con base a una posicón en el arreglo "i".*/
  eliminar(i: number): void {
    Swal.fire({
      title: 'Eliminar',
      text: `¿Estás seguro que deseas eliminar al usuario ${this.usuarios[i].nombre}?`,
      icon: 'question',
      showCancelButton: true,
      showConfirmButton: true
    }).then( resp => {
      if ( resp.value ) {

        this.user_service.eliminarUsuario(i);

        this.consultarInformacion();
        
      }
    });
  }

}
