import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';


/**
 * Componente donde se crea un nuevo registro.
 * 
 * - Contiene los campos necesarios y todos son obligatorios para crear un usuario. También se debe de seleccionar una imagen para poder avanzar.
 * - Contiene un botón de cancelar el registro (Parte superior izquierda "<-") que regresa al inicio del programa (No regresa al páginado donde se muestran todos los usuarios). 
 * 
 */

export interface Usuario {
  nombre: string,
  apellidos: string,
  direccion: string,
  correo: string,
  telefono: number,
  imagen: any,
}
interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget | null;
}

export interface ImagUser {
  nombre: String,
  data: any,
}


@Component({
  selector: 'app-cru-usuario',
  templateUrl: './cru-usuario.component.html',
  styleUrls: ['./cru-usuario.component.css']
})
export class CruUsuarioComponent implements OnInit {


  formUser: FormGroup;

  selectedFile: any;

  imagenSeleccionada: ImagUser; 

  valueImagen = '';
  base64textString = '';
  nombreImagen: string = '';

  cambiandoImagen: boolean = false;

  constructor(private fb: FormBuilder, private user_service: UserService, public router: Router) {

    
    this.imagenSeleccionada = {nombre: '', data: undefined}; 
    this.formUser = this.fb.group({
      nombre: new FormControl('', [Validators.required]),
      apellidos: new FormControl('', [Validators.required]),
      direccion: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      telefono: new FormControl('', [Validators.required, Validators.pattern('[- +()0-9]+')]),
    });
  }
  // Las siguientes 5 funciones son para obtener las validaciones que se hicieron al grupo de captura. 
  get nombre() {
    return this.formUser.controls['nombre'] as FormControl;
  }
  get apellidos() {
    return this.formUser.controls['apellidos'] as FormControl;
  }
  get direccion() {
    return this.formUser.controls['direccion'] as FormControl;
  }
  get email() {
    return this.formUser.controls['email'] as FormControl;
  }
  get telefono() {
    return this.formUser.controls['telefono'] as FormControl;
  }

  

  ngOnInit(): void {
    
   
  }
  // Método para solicitar el registro de los datos capturados. También se valida que se haya elegido una imagen. 
  // Una vez guardada la información se redirige a visualizar todos los registros. 
  enviarInformacion() {
    if (this.imagenSeleccionada.nombre == "" ) {
      Swal.fire({
        title: 'Atención',
        text: "Selecciona imagen.",
        icon: "warning",
      });

      return;
    } 

    if ( this.formUser.invalid ) {
      Swal.fire({
        title: 'Error',
        text: "Información no válida.",
        icon: "error",
      });
      return;
    }

    let newUser: Usuario;

    newUser = {
      nombre: this.nombre.value,
      apellidos: this.apellidos.value,
      direccion: this.direccion.value,
      correo: this.email.value,
      telefono: this.telefono.value,
      imagen: this.imagenSeleccionada
    }

    this.user_service.agregarUsuario(newUser);

    this.router.navigate(['usuarios']);

  }

  // Método para obtener la imagen seleccionada. 
  addImge(fileInput: any) {
    let file;

    this.cambiandoImagen = true;

    if (fileInput.target != null && fileInput.target.files != null && (file = fileInput.target.files[0]) && (file.type === 'image/png' || file.type === 'image/jpeg'|| file.type === 'image/jpg')) {
      const file = fileInput.target.files[0];
      const fileName = file.name;

      if ( !fileName) {
        return;
      }
      this.valueImagen = fileName;

      

      const reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    } else {
      Swal.fire({
        title: 'Error',
        text: 'Selecciona un archivo válido. Solo se permite jpeg o png.',
        icon: 'error'
      }).then( ()=> {
        this.imagenSeleccionada.data = undefined;
        this.imagenSeleccionada.nombre = '';
        this.nombreImagen = '';
        this.valueImagen = '';
        this.base64textString = '';
        this.cambiandoImagen = false;
      });
    }
    
  }
  // Método auxiliar que ayuda a procesar la imagen y obtener los datos en base64
  _handleReaderLoaded(readerEvt: any) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);

    this.imagenSeleccionada.data = this.base64textString;
    this.imagenSeleccionada.nombre = this.valueImagen;
    this.cambiandoImagen = false;
   }

}
