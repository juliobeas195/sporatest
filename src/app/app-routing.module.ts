import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login/login.component';
import { CruUsuarioComponent } from './components/usuario/cru-usuario/cru-usuario.component';
import { HomeUsuarioComponent } from './components/usuario/home-usuario/home-usuario.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'usuario/:id', component: CruUsuarioComponent},
  {path: 'usuarios', component: HomeUsuarioComponent},
  // {path: 'management', component: ProfileHomeComponent, canActivate: [AuthGuard] },
  { path: '', component: LoginComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
